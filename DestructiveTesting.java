import java.util.*;
import java.math.*;

public class DestructiveTesting {
   private static long maxH, safeH, brokeH1, brokeH2, numDrops = 0;

   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);
      long result;

      System.out.print("Enter ladder height: ");
      while ((maxH = input.nextLong()) < 1)
         System.out.println("Must be >= 1");

      System.out.print("Enter highest safe rung: ");
      while ((safeH = input.nextLong()) > maxH || safeH < 1)
         System.out.println("Must be >= 1 and <= " + maxH);

      System.out.println("The inputs were " + maxH + ", " + safeH);
      result = run();

      System.out.println("----Run Results----");
      System.out.println("Highest safe rung determined by this experiment: " + result);
      System.out.println("Rung where the first test device broke: " + brokeH1);
      System.out.println("Rung where the second test device broke: " + brokeH2);
      System.out.println("Total number of drops: " + numDrops);
   }

   private static long run() {
      long h = 1, prevH = 1, inc;

      inc = (int)Math.sqrt((double)maxH);   //Increment by sqrt(n)
      while (drop(h)) {
         prevH = h;
         h += inc;
         numDrops++;
      }
      brokeH1 = h;
      h = prevH;

      while (drop(h)) {
         h++;
         numDrops++;
      }
      brokeH2 = h;

      return --h;
   }

   private static boolean drop(long h) {
      boolean ok = true;

      if (h > safeH)
         ok = false;

      return ok;
   }

}
